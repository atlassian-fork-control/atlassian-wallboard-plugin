#!/bin/bash

set -u
set -o nounset

WB_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# needed by exit_with_error()
trap "exit 1" TERM
export PID=$$

function exit_with_error() {
    kill -s TERM $PID
}

function die() {
    local msg=${1:-}
    if [ -n "$msg" ]; then
        echo "$msg" >&2
    else
        echo "Error running last command!" >&2
    fi

    exit_with_error
}

# try to find Maven
if [ -f "${M2_HOME:-}/bin/mvn" ]; then
  MVN=${M2_HOME}/bin/mvn
elif [ -f "/opt/java/tools/maven/apache-maven-2.1.0/bin/mvn" ]; then
  MVN=/opt/java/tools/maven/apache-maven-2.1.0/bin/mvn
else
  MVN=mvn
fi

# the branches to release WB from
WB_BRANCH="default"

function banner() {
    echo ">"
    echo "> $*"
    echo ">"
}

function print_usage() {
    echo "usage: release.sh [--dry-run]" >&2
}

function wb_prepare_release() {
    banner "Preparing WB release"

    local dry_run=$1
    local dir=${WB_DIR}
    local args="-Darguments='-DskipITs'"

    local release_args=''
    if [ -n "${dry_run}" ]; then
        release_args="-DdryRun=true"
    fi

    pushd "${dir}"
    ${DEBUG} "${MVN}" -B ${release_args} ${args} release:prepare || die "Failed to release:prepare in $dir"
    popd
}

function get_release_tag() {
    local dir=$1

    echo `cat "${dir}"/release.properties | grep "^scm.tag=" | cut -b 9-` # the SCM tag
}

function wb_perform_release() {
    local dry_run=$1
    local rel_tag=$2
    local rel_dir=${WB_DIR}

    banner "Performing WB release"

    # try to use Bamboo's deployment credentials if available
    local settings=""
    if [ -f /opt/bamboo-agent/.m2/jira-deploy-settings.xml ]; then
        settings="-s /opt/bamboo-agent/.m2/jira-deploy-settings.xml"
    fi  # otherwise use default

    [ -n "${dry_run}" ] && { echo "Dry run requested, skipping perform_release of '${rel_tag}' in ${rel_dir}"; return; }
    pushd "${rel_dir}"
    hg update "${rel_tag}" || die "Failed to checkout '${rel_tag}' in ${rel_dir}"
    ${DEBUG} "${MVN}" ${settings} -B release:perform -DskipTests -Darguments=-DskipTests || die "Failed to mvn deploy in ${rel_dir}"
    popd
}

function wb_get_jira_version() {
    echo `grep "<jira.version>.*</jira.version>" ${WB_DIR}/pom.xml | sed -e 's/.*<jira.version>\(.*\)<\/jira.version>.*/\1/'`
}

# get command line parameters
DEBUG=""
DRY_RUN=""
while [ $# -ne 0 ] ; do
    arg=$1
    shift

    case $arg in
    "--dry-run")
        DRY_RUN="yes" ;;
    "-d")
        DEBUG="echo" ;;
    *)
        print_usage; exit 1 ;;
    esac
done

# bail if no Maven
[ -x `which "${MVN}"` ] || { echo "Maven not found in \$PATH or \$M2_HOME}!" >&2; exit 1; }

# figure out if we are depending on a SNAPSHOT version of JIRA
WB_JIRA_VERSION=`wb_get_jira_version`
WB_USING_SNAPSHOT=`echo $WB_JIRA_VERSION | grep -- '-SNAPSHOT$'`

if [ -n "$WB_USING_SNAPSHOT" ]; then
    die "Wallboards is using a SNAPSHOT JIRA version"
fi

# now we can cut a WB release
hg update "${WB_BRANCH}"
wb_prepare_release "${DRY_RUN}"
WB_TAG=`get_release_tag "${WB_DIR}"`
wb_perform_release "${DRY_RUN}" "${WB_TAG}"