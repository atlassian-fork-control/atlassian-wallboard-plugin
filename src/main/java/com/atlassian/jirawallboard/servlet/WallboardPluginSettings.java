package com.atlassian.jirawallboard.servlet;

import com.atlassian.annotations.tenancy.TenancyScope;
import com.atlassian.annotations.tenancy.TenantAware;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.json.JSONArray;
import com.atlassian.jira.util.json.JSONException;
import com.atlassian.jira.util.json.JSONObject;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

@TenantAware(TenancyScope.TENANTED)
public class WallboardPluginSettings
{
    static final String WALLBOARD_KEY = "wallboardPluginSettingsKey:";
    private boolean isConfigured = true;
    private List<String> dashboardIds = new ArrayList<>();
    private String transitionFx = "none";
    private int cyclePeriod = 30;
    private boolean random = false;
    private final PluginSettingsFactory pluginSettingsFactory;
    private final String userKey;

    public static final Logger log = Logger.getLogger(WallboardPluginSettings.class);

    public WallboardPluginSettings(PluginSettingsFactory pluginSettingsFactory, ApplicationUser user)
    {
        this.pluginSettingsFactory = pluginSettingsFactory;
        this.userKey = user != null ? user.getKey() : null;
    }

    static public WallboardPluginSettings loadSettings(PluginSettingsFactory pluginSettingsFactory, String username)
    {
        return loadSettings(pluginSettingsFactory, ComponentAccessor.getUserManager().getUserByName(username));
    }

    static public WallboardPluginSettings loadSettings(PluginSettingsFactory pluginSettingsFactory, ApplicationUser user)
    {
        WallboardPluginSettings settings = new WallboardPluginSettings(pluginSettingsFactory, user);
        Object val = pluginSettingsFactory.createGlobalSettings().get(WALLBOARD_KEY + mapNullToBlank(settings.userKey));
        if (val == null)
        {
            settings.isConfigured = false;
            return settings;
        }
        JSONObject jsonRepresentation;
        try
        {
            jsonRepresentation = new JSONObject((String) val);
            JSONArray rawDashboardIds = jsonRepresentation.getJSONArray("dashboardIds");
            settings.dashboardIds = new ArrayList<>(rawDashboardIds.length());
            for (int i = 0; i < rawDashboardIds.length(); i++)
            {
                settings.dashboardIds.add(i, (String) rawDashboardIds.get(i));
            }
            settings.setCyclePeriod(jsonRepresentation.getInt(WallboardServlet.CYCLE_PERIOD.getKey()));
            settings.setTransitionFx(jsonRepresentation.getString(WallboardServlet.TRANSITION_FX.getKey()));
            settings.setRandom(jsonRepresentation.getBoolean(WallboardServlet.RANDOM.getKey()));

        }
        catch (JSONException e)
        {
            settings.isConfigured = false;
        }
        return settings;
    }

    public List<String> getDashboardIds()
    {
        return new ArrayList<>(dashboardIds);
    }

    public void setDashboardIds(List<String> newDashboardIds)
    {
        dashboardIds = new ArrayList<>(newDashboardIds.size());
        for (String s : newDashboardIds)
        {
            dashboardIds.add(s);
        }
    }

    public String getTransitionFx()
    {
        return transitionFx;
    }

    public void setTransitionFx(String transitionFx)
    {
        this.transitionFx = transitionFx;
    }

    public int getCyclePeriod()
    {
        return cyclePeriod;
    }

    public void setCyclePeriod(int cyclePeriod)
    {
        this.cyclePeriod = cyclePeriod;
    }

    public boolean isRandom()
    {
        return random;
    }

    public void setRandom(boolean random)
    {
        this.random = random;
    }

    public boolean isConfigured()
    {
        return isConfigured;
    }

    public void saveChanges()
    {
        JSONObject object = new JSONObject();
        JSONArray array = new JSONArray();
        for (String s : dashboardIds)
        {
            array.put(s);
        }
        try
        {
            object.put("dashboardIds", array);
            object.put(WallboardServlet.CYCLE_PERIOD.getKey(), getCyclePeriod());
            object.put(WallboardServlet.RANDOM.getKey(), isRandom());
            object.put(WallboardServlet.TRANSITION_FX.getKey(), getTransitionFx());
        }
        catch (JSONException e)
        {
            log.log(Level.ERROR, "JSONException in saving changes to wallboard plugin settings:" + e.getMessage());
        }

        pluginSettingsFactory.createGlobalSettings().put(WALLBOARD_KEY + mapNullToBlank(userKey), object.toString());
        isConfigured = true;
    }

    private static String mapNullToBlank(String userKey)
    {
        return (userKey != null) ? userKey : "";
    }

}
