package com.atlassian.jirawallboard.servlet;

import com.atlassian.jirawallboard.layout.WallboardColumn;
import com.atlassian.templaterenderer.annotations.HtmlSafe;

import java.util.ArrayList;
import java.util.List;

public class WallframeParams
{

    private List<WallboardColumn> cols = new ArrayList<WallboardColumn>(3);
    private List<Width> columnWidths = new ArrayList<Width>(3);

    private WallboardMetadata wallboardMetadata;

    private int numCols;
    private static final int MAX_COLUMNS = 3;
    private String resourceIncludes;
    //private int refreshSchedule;

    public WallframeParams()
    {

        this.wallboardMetadata = null;
        this.numCols = 0;
        //this.refreshSchedule = 10 * 60; //Default of 10 minutes
    }

    public WallboardMetadata getWallboardMetadata()
    {
        return wallboardMetadata;
    }

    public void addColumn(WallboardColumn col)
    {
        if (numCols < MAX_COLUMNS)
        {
            this.cols.add(this.numCols, col);
            this.numCols++;
        }
    }

    public void setWallboardMetadata(WallboardMetadata wallboardMetadata)
    {
        this.wallboardMetadata = wallboardMetadata;
    }

    public List<WallboardColumn> getCols()
    {
        return cols;
    }

    public int getNumCols()
    {
        return numCols;
    }

    public List<Width> getColumnWidths()
    {
        return columnWidths;
    }

    public void setColumnWidths(List<Width> columnWidths)
    {
        this.columnWidths = columnWidths;
    }

    @HtmlSafe
    public String getResourceIncludes()
    {
        return resourceIncludes;
    }

    public void setResourceIncludes(String resourceIncludes)
    {
        this.resourceIncludes = resourceIncludes;
    }

    static public class WallboardMetadata
    {
        private String baseUrl;
        private static final String PLUGIN_KEY = "com.atlassian.jirawallboard.atlassian-wallboard-plugin";
        private static final String SERVLET_KEY = "wallboard";

        public WallboardMetadata(String baseUrl)
        {
            this.baseUrl = baseUrl;
        }

        public String getBaseUrl()
        {
            return baseUrl;
        }

        public String getPluginKey()
        {
            return PLUGIN_KEY;
        }

        public String getServletKey()
        {
            return SERVLET_KEY;
        }
    }

    public enum Width
    {
        NARROW,
        MEDIUM_WIDTH,
        WIDE,
        FULL_WIDTH
    }

}