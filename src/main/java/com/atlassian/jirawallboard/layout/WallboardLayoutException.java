package com.atlassian.jirawallboard.layout;

/**
 * User: jhatherly
 * Date: 20/08/2010
 * Time: 1:36:54 PM
 */
public class WallboardLayoutException extends RuntimeException {

    public WallboardLayoutException(String s) {
        super(s);
    }
}
