package com.atlassian.jirawallboard.layout;

import com.atlassian.gadgets.dashboard.Color;
import com.atlassian.gadgets.dashboard.DashboardItemRepresentation;
import com.atlassian.jira.util.json.JSONEscaper;
import com.atlassian.velocity.htmlsafe.HtmlSafe;
import com.google.common.base.Function;
import com.google.common.collect.Ordering;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import javax.annotation.Nullable;

import static com.atlassian.jirawallboard.layout.WallboardColumn.WallboardItem.dashboardItem;
import static com.atlassian.jirawallboard.layout.WallboardColumn.WallboardItem.openSocialGadget;

public class WallboardColumn
{

    private List<WallboardItemCollection> itemSlots;
    private static final int ITEM_DEFAULT_HEIGHT = 300;

    public WallboardColumn()
    {
        this.itemSlots = new LinkedList<WallboardItemCollection>();
    }

    public void addGadget(String url, Color colour, boolean isWallboardable, String id, int height)
    {
        addWallboardItem(openSocialGadget(url, isWallboardable, id, height), colour);
    }

    public void addDashboardItem(DashboardItemRepresentation representation, Color color, String id)
    {
        if (representation.getHtml().isDefined() || representation.getAmdModule().isDefined())
        {
            addWallboardItem(
                    dashboardItem(representation.getHtml().getOrNull(), representation.getAmdModule().getOrNull(), representation.getJsonRepresentation().get(), id),
                    color);
        }
        else if (representation.getGadgetUrl().isDefined())
        {
            addGadget(representation.getGadgetUrl().get(), color, true, id, 0);
        }
    }

    private void addWallboardItem(WallboardItem descriptor, Color color)
    {
        WallboardItemCollection back = null;
        if (itemSlots.size() > 0)
        {
            back = itemSlots.get(itemSlots.size() - 1);
        }
        if (back != null && back.getColour() == color)
        {
            back.add(descriptor);
        }
        else
        {
            WallboardItemCollection newSlot = new WallboardItemCollection(color);
            itemSlots.add(newSlot);
            newSlot.add(descriptor);
        }

    }

    public List<WallboardItemCollection> getItemSlots()
    {
        return itemSlots;
    }

    public static class WallboardItem
    {
        private final String url;
        private final String html;
        private final String amdModule;
        private final String jsonRepresentation;
        private final boolean isWallboardable;
        private final String id;
        private final int height;
        private final boolean spacer;

        private WallboardItem(final String url, final String html, final String amdModule, final String jsonRepresentation, final boolean isWallboardable, final String id, final int height)
        {
            this.url = url;
            this.html = html;
            this.amdModule = amdModule;
            this.jsonRepresentation = jsonRepresentation;
            this.isWallboardable = isWallboardable;
            this.id = id;
            this.spacer = url != null && url.contains("spacerGadget.xml");
            if (height <= 0)
            {
                this.height = ITEM_DEFAULT_HEIGHT;
            }
            else
            {
                this.height = height;
            }
        }

        @HtmlSafe
        public String getUrl()
        {
            return url;
        }

        public boolean isWallboardable()
        {
            return isWallboardable;
        }

        public String getId()
        {
            return id;
        }

        public int getHeight()
        {
            return height;
        }

        public boolean isSpacer()
        {
            return spacer;
        }

        public String getHtml()
        {
            return html;
        }

        public String getAmdModule()
        {
            return amdModule;
        }

        public String getHtmlJsonEscaped()
        {
            return JSONEscaper.escape(html);
        }

        @HtmlSafe
        public String getJsonRepresentation()
        {
            return jsonRepresentation;
        }

        public static WallboardItem dashboardItem(@Nullable final String html, @Nullable String amdModule, final String jsonRepresentation, final String id)
        {
            return new WallboardItem(null, html, amdModule, jsonRepresentation, true, id, 300);
        }

        public static WallboardItem openSocialGadget(String url, boolean wallboard, String id, int height)
        {
            return new WallboardItem(url, null, null, null, wallboard, id, height);
        }
    }

    public class WallboardItemCollection implements Iterable<WallboardItem>
    {

        private final Collection<WallboardItem> items;
        private final Color colour;

        public WallboardItemCollection(Color colour)
        {
            this.items = new ArrayList<WallboardItem>();
            this.colour = colour;

        }

        public void add(WallboardItem item)
        {
            items.add(item);
        }

        public Iterator<WallboardItem> iterator()
        {
            return items.iterator();
        }

        public Color getColour()
        {
            return colour;
        }

        public int getHeight()
        {
            return Ordering.natural().onResultOf(new Function<WallboardItem, Comparable>()
            {
                public Comparable apply(final WallboardItem input)
                {
                    return input.getHeight();
                }
            }).max(items).getHeight();
        }

        public boolean isSpacerOnly()
        {
            return items.size() == 1 && items.iterator().next().isSpacer();
        }
    }
}