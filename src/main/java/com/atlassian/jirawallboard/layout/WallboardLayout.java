package com.atlassian.jirawallboard.layout;

import com.atlassian.annotations.tenancy.TenancyScope;
import com.atlassian.annotations.tenancy.TenantAware;
import com.atlassian.gadgets.DashboardItemState;
import com.atlassian.gadgets.GadgetRequestContext;
import com.atlassian.gadgets.GadgetState;
import com.atlassian.gadgets.dashboard.DashboardItemRepresentation;
import com.atlassian.gadgets.dashboard.Layout;
import com.atlassian.gadgets.spec.GadgetSpec;
import com.atlassian.gadgets.view.RenderedGadgetUriBuilder;
import com.atlassian.gadgets.view.View;
import com.atlassian.gadgets.view.ViewType;
import com.atlassian.jirawallboard.servlet.WallframeParams;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;

import javax.annotation.Nullable;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

public class WallboardLayout
{
    private final GadgetRequestContext gadgetRequestContext;
    private final View wallboardView;
    private final RenderedGadgetUriBuilder renderedGadgetUriBuilder;
    private final List<WallframeParams.Width> widths;
    private final List<WallboardColumn> columns;

    public static final String PARAMS = "params";

    @TenantAware(TenancyScope.TENANTLESS)
    private static final ImmutableMap<Layout, ImmutableList<WallframeParams.Width>> WIDTHS = ImmutableMap.of(
            Layout.A, ImmutableList.of(WallframeParams.Width.FULL_WIDTH),
            Layout.AB, ImmutableList.of(WallframeParams.Width.WIDE, WallframeParams.Width.NARROW),
            Layout.BA, ImmutableList.of(WallframeParams.Width.NARROW, WallframeParams.Width.WIDE),
            Layout.AA, ImmutableList.of(WallframeParams.Width.MEDIUM_WIDTH, WallframeParams.Width.MEDIUM_WIDTH),
            Layout.AAA, ImmutableList.of(WallframeParams.Width.NARROW, WallframeParams.Width.NARROW, WallframeParams.Width.NARROW));

    public WallboardLayout(GadgetRequestContext gadgetRequestContext,
                           View wallboardView,
                           RenderedGadgetUriBuilder renderedGadgetUriBuilder,
                           Layout layout)
    {
        this.gadgetRequestContext = gadgetRequestContext;
        this.wallboardView = wallboardView;
        this.renderedGadgetUriBuilder = renderedGadgetUriBuilder;
        this.widths = WIDTHS.get(layout);
        this.columns = new ArrayList<WallboardColumn>(widths.size());
        for (WallframeParams.Width width : widths)
        {
            this.columns.add(new WallboardColumn());
        }

    }

    public void addGadget(int column, GadgetState gadget, GadgetSpec gadgetSpec) throws WallboardLayoutException
    {
        getColumn(column).addGadget(this.generateGadgetUrl(gadget, gadgetSpec), gadget.getColor(), this.supportsWallboardView(gadgetSpec), gadget.getId().toString(), gadgetSpec.getHeight());
    }

    public void addDashboardItem(int column, DashboardItemState itemState, DashboardItemRepresentation representation) throws WallboardLayoutException
    {
        getColumn(column).addDashboardItem(representation, itemState.getColor(), itemState.getId().value());
    }

    private WallboardColumn getColumn(int column)
    {
        if (column >= this.columns.size() || column < 0)
        {
            throw new WallboardLayoutException("Column index out of layout's bounds");
        }
        else
        {
            return this.columns.get(column);
        }
    }

    public WallframeParams generateParams(String contextPath)
    {
        WallframeParams params = new WallframeParams();
        for (WallboardColumn column : this.columns)
        {
            params.addColumn(column);
        }
        params.setWallboardMetadata(new WallframeParams.WallboardMetadata(contextPath));
        params.setColumnWidths(widths);

        return params;
    }

    @Nullable
    protected String generateGadgetUrl(GadgetState gadget, GadgetSpec gadgetSpec)
    {
        URI uri = null;
        if (gadgetSpec.supportsViewType(this.wallboardView.getViewType()))
        {
            uri = renderedGadgetUriBuilder.build(gadget, this.wallboardView, gadgetRequestContext);
        }
        else if (gadgetSpec.supportsViewType(ViewType.DEFAULT))
        {
            uri = renderedGadgetUriBuilder.build(gadget, View.DEFAULT, gadgetRequestContext);
        }
        return uri == null ? null : uri.toString();
    }

    protected boolean supportsWallboardView(GadgetSpec gadgetSpec)
    {
        return gadgetSpec.supportsViewType(this.wallboardView.getViewType());
    }
}
