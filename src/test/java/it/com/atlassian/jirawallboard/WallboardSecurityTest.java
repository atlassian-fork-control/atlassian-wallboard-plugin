package it.com.atlassian.jirawallboard;

import com.atlassian.jira.pageobjects.pages.AddDashboardPage;
import com.atlassian.jira.testkit.client.JIRAEnvironmentData;
import com.atlassian.jira.testkit.client.RestApiClient;
import com.sun.jersey.api.client.ClientResponse;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.core.MediaType;
import java.util.Locale;

import static org.junit.Assert.assertTrue;

public class WallboardSecurityTest extends AbstractWallboardTest {

    @Before
    public void setup() {
        jira.backdoor().restoreBlankInstance();
        jira.backdoor().flags().clearFlags();
        jira.backdoor().darkFeatures().enableForSite("jira.onboarding.feature.disabled");

        final AddDashboardPage addDashboardPage = jira.quickLoginAsAdmin(AddDashboardPage.class);
        addDashboardPage.setName("test dashboard");
        addDashboardPage.submit();
    }

    private static class WallboardClient extends RestApiClient<WallboardClient> {

        WallboardClient(JIRAEnvironmentData environmentData) {
            super(environmentData);
        }

        ClientResponse sendMaliciousRequest() {

            String encodedMaliciousForm =
                    String.format(
                            "inline=true" +
                                    "&decorator=dialog" +
                                    "&dashboardId=%d" +
                                    "&random=true" +
                                    "&transitionFx=none" +
                                    "&cyclePeriod=30",
                            EMPTY_DASHBOARD_ID);

            return resourceRoot(getEnvironmentData().getBaseUrl().toExternalForm())
                    .path("ConfigureWallboard.jspa")
                    .accept(MediaType.TEXT_HTML)
                    .type(MediaType.APPLICATION_FORM_URLENCODED)
                    .entity(encodedMaliciousForm)
                    .post(ClientResponse.class);
        }
    }

    private static final int EMPTY_DASHBOARD_ID = 10010;

    @Test
    public void configureWallboardFormIsXsrfResistant() {
        WallboardClient client = new WallboardClient(jira.environmentData());
        ClientResponse response = client.sendMaliciousRequest();
        String expectedXsrfErrorTitle = jira.backdoor().i18n().getText("xsrf.error.title", Locale.getDefault().toString());
        assertTrue(
                "Submitting a configure wallboard form without a token should fail the XSRF check",
                response.getEntity(String.class).contains(expectedXsrfErrorTitle));
    }
}
