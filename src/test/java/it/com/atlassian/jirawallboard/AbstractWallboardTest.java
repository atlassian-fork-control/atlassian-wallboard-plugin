package it.com.atlassian.jirawallboard;

import com.atlassian.jira.pageobjects.BaseJiraWebTest;
import com.atlassian.jira.pageobjects.JiraTestedProduct;
import com.atlassian.jira.pageobjects.pages.DashboardPage;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.webdriver.testing.annotation.WindowSize;
import org.openqa.selenium.By;

import javax.inject.Inject;

@WindowSize (width = 1024, height = 1000)
abstract class AbstractWallboardTest extends BaseJiraWebTest
{
    public static final By WALLBOARD_LINK = By.id("wallboard_link");
    public static final By WALLBOARD_PAGE = By.id("wallboards");

    @Inject
    protected PageElementFinder finder;

    @Inject
    protected PageBinder pageBinder;

    @Inject
    protected JiraTestedProduct jira;

    protected final void gotoWallboardView()
    {
        DashboardPage dashboardPage = pageBinder.bind(DashboardPage.class);
        dashboardPage.viewAsWallboard();
    }
}
