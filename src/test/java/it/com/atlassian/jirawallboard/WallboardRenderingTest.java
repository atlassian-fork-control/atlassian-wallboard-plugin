package it.com.atlassian.jirawallboard;

import com.atlassian.jira.pageobjects.pages.AddDashboardPage;
import com.atlassian.jira.pageobjects.pages.DashboardPage;
import com.atlassian.jira.webtest.webdriver.util.AUIBlanket;
import com.google.inject.Inject;
import it.com.atlassian.gadgets.pages.Gadget;
import it.com.atlassian.gadgets.pages.GadgetsDashboardPage;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;

/**
 * This test checks if dashboard items are rendered properly on the wallboard
 * and that the correct view is provided for them.
 */
public class WallboardRenderingTest extends AbstractWallboardTest
{
    private static final String SERVER_SIDE_DASHBOARD_ITEM_NAME = "Server-side with AMD module";
    private static final String LOCAL_DASHBOARD_ITEM_NAME = "New local dashboard item";
    private static final String DASHBOARD_ITEM_NOT_DISPLAYABLE_ON_WALLBOARD = "Dashboard item without wallboard support";
    public static final String ISSUES_IN_PROGRESS_GADGET_NAME = "Issues in progress";
    public static final String DASHBOARD_ITEM_WITH_DESCRIPTION_THAT_REPLACES_GADGET = "New local dashboard item that replaces \"The title of this gadget will be replaced\"";

    public static final int EMPTY_DASHBOARD_ID = 10010;

    private GadgetsDashboardPage dashboardPage;

    @Inject
    private AUIBlanket auiBlanket;

    @Before
    public void setup()
    {
        jira.backdoor().restoreBlankInstance();
        jira.backdoor().flags().clearFlags();
        jira.backdoor().darkFeatures().enableForSite("jira.onboarding.feature.disabled");

        final AddDashboardPage addDashboardPage = jira.quickLoginAsAdmin(AddDashboardPage.class);
        addDashboardPage.setName("test dashboard");
        addDashboardPage.submit();
        jira.goTo(DashboardPage.class, EMPTY_DASHBOARD_ID);
        dashboardPage = pageBinder.bind(GadgetsDashboardPage.class, EMPTY_DASHBOARD_ID);
    }

    @Test
    public void javaScriptDashboardItemIsRendered() throws Exception
    {
        Gadget item = addItem(LOCAL_DASHBOARD_ITEM_NAME);
        gotoWallboardView();

        assertThatLocalDashboardItemIsRendered(item);
    }

    @Test
    public void serverSideDashboardItemIsRendered() throws Exception
    {
        Gadget item = addItem(SERVER_SIDE_DASHBOARD_ITEM_NAME);
        gotoWallboardView();

        assertThat("proper context is passed to the template", item.getText(),
                containsString(String.format("Hello world from a server-side dashboard item %s in dashboard %s (view: WALLBOARD)", item.getId().replace("gadget-", ""), EMPTY_DASHBOARD_ID)));
        assertThat("amd module is called", item.getText(),
                containsString("This dashboard-item got rendered client-side."));
    }

    @Test
    public void openSocialDashboardItemIsRenderedAsOpenSocialGadget() throws Exception
    {
        Gadget item = addItem(ISSUES_IN_PROGRESS_GADGET_NAME);
        gotoWallboardView();

        assertThatOpenSocialGadgetIsRendered(item);
    }

    @Test
    public void dashboardItemWithDescriptorReplacingOpenSocialGadgetIsRenderedInTheNewWay() throws Exception
    {
        Gadget item = addItem(DASHBOARD_ITEM_WITH_DESCRIPTION_THAT_REPLACES_GADGET);
        gotoWallboardView();

        assertThat(item.getText(), containsString("This dashboard-item got rendered client-side."));
    }

    @Test
    public void bothOpenSocialGadgetsAndDashboardItemsCanBeRenderedOnWallboard() throws Exception
    {
        Gadget item = addItem(LOCAL_DASHBOARD_ITEM_NAME);
        Gadget gadget = addItem(ISSUES_IN_PROGRESS_GADGET_NAME);
        gadget.dragTo(dashboardPage.getColumn(1));
        gotoWallboardView();

        assertThatLocalDashboardItemIsRendered(item);
        assertThatOpenSocialGadgetIsRendered(gadget);
    }

    @Test
    public void dashboardItemWithUnsatisfiedConditionIsNotRendered()
    {
        Gadget item = addItem(DASHBOARD_ITEM_NOT_DISPLAYABLE_ON_WALLBOARD);
        Gadget item2 = addItem(LOCAL_DASHBOARD_ITEM_NAME);

        assertThat(dashboardPage.getGadgetsForColumn(0), hasSize(2));

        gotoWallboardView();

        assertThat(dashboardPage.gadgetExists(item.getId()), equalTo(false));
        assertThat(dashboardPage.gadgetExists(item2.getId()), equalTo(true));
    }

    private Gadget addItem(final String itemName) {
        Gadget item = dashboardPage.addItem(itemName);
        auiBlanket.waitUntilClosed();
        return item;
    }

    private void assertThatLocalDashboardItemIsRendered(final Gadget item)
    {
        assertThat("item is rendered", item.getText(), containsString("This dashboard-item got rendered client-side."));
        assertThat("items on dashboard are not editable", item.getText(), containsString("Editable: false"));
        assertThat("the view is WALLBOARD", item.getText(), containsString("View: WALLBOARD"));
    }

    private void assertThatOpenSocialGadgetIsRendered(final Gadget item) {assertThat("original gadget is displayed in an iFrame", dashboardPage.getGadget(item.getId()).getHtml().toLowerCase(), containsString("iframe"));}
}
